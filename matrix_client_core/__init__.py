# stdlib
import json
import getpass
import re
import itertools
import sys
import time
import queue
import threading

# external deps
import matrix_client.client
import requests

# in-tree deps
import matrix_client_core.notifier as notifier
from .exception import wrap_exception, ExceptionHandler
from .repl import ConsoleClientMixin


class CFException(Exception):
	pass


class AccountInfo:
	T_UNDEF = 0
	T_TOKEN = 1
	T_PASSWORD = 2

	def __init__(self):
		self.hs_client_api_url = None
		self.mxid = None
		self.access_token = None
		self.password = None

	def loadfromfile(self, filename):
		with open(filename, "r") as f:
			j = json.load(f)
			self.hs_client_api_url = j['hs_client_api_url']
			self.mxid = j['mxid']
			self.access_token = j['access_token']
		return True

	def savetofile(self, filename):
		d = {
			'hs_client_api_url': self.hs_client_api_url,
			'mxid': self.mxid,
			'access_token': self.access_token	}

		with open(filename, "w") as f:
			s = json.dumps(d, sort_keys=True,
			 indent=4, separators=(',', ': '))
			f.write(s)
		return True

	def _ask(self, question, default, reask, reuse):
		if not reask and default is not None: return default
		if reuse and default is not None:
			prompt = "{0} [{1}]: ".format(question, default)
		else:
			prompt = "{0}: ".format(question)
		answer = input(prompt)
		if reuse and answer == "":
			answer = default
		return answer

	def _askpass(self, question, default, reask):
		if not reask and default is not None: return default
		prompt = "{0}: ".format(question)
		return getpass.getpass(prompt)

	def getfromkeyboard(self, reask=False, reuse=True):
		self.hs_client_api_url = self._ask("Homeserver URL",
							self.hs_client_api_url,
							reask, reuse).rstrip("/")
		self.mxid = self._ask("User ID", self.mxid, reask, reuse)
		self.password = self._askpass("Password", self.password, reask)

	def login_type(self):
		if not self.hs_client_api_url: return self.T_UNDEF
		if not self.mxid: return self.T_UNDEF
		if self.access_token: return self.T_TOKEN
		if self.password: return self.T_PASSWORD
		return self.T_UNDEF

	def __bool__(self):
		return bool(self.login_type())


class RoomList:
	RE_PREFIX = re.compile("^(#[^:]*)")

	def __init__(self, roomsdict):
		# 'roomsdict' should be a dictionary mapping room IDs to room objects,
		# as returned by MatrixClient.get_rooms()

		self.roomsbyid = roomsdict
		self.roomsbyalias = {}
		self.roomsbyprefix = {}	# Note: Can contain list for multiple matches

		for r in roomsdict.values():
			for alias in itertools.chain((r.canonical_alias,), r.aliases):
				if alias is None: continue
				self.roomsbyalias[alias] = r
				m = self.RE_PREFIX.search(alias)
				if not m: continue
				prefix = m.group(1)
				item = self.roomsbyprefix.get(prefix)
				if item is None:
					self.roomsbyprefix[prefix] = r
				elif isinstance(item, list):
					if r not in item: item.append(r)
				elif item != r:
					self.roomsbyprefix[prefix] = [item, r]

	def get_room(self, id_or_alias_or_prefix):
		# Find a room object by ID or alias, and return it

		# We also happen to guarantee that if the result is
		# not None, 'id_or_alias_or_prefix' _uniquely_ identifies the room.

		if id_or_alias_or_prefix in self.roomsbyid:
			return self.roomsbyid[id_or_alias_or_prefix]
		if id_or_alias_or_prefix in self.roomsbyalias:
			return self.roomsbyalias[id_or_alias_or_prefix]
		if id_or_alias_or_prefix in self.roomsbyprefix:
			x = self.roomsbyprefix[id_or_alias_or_prefix]
			try:
				# if it has a length, it's probably a list of rooms
				if len(x) == 1: return x[0]
				return None # Multiple matches is no match
			except TypeError: # x doesn't _have_ a length
				return x  # so probably a room object
		return None

	@staticmethod
	def _best_handle(*args):
		# Return shortest argument which is not None (if any)
		best_match = None
		for a in args:
			if a is None: continue
			if best_match is None \
			   or len(a) < len(best_match): best_match = a
		return best_match

	def get_room_handle(self, id_or_alias_or_prefix):
		# get a convenient short display handle for the room
		# always returns something useful, even if just unmodified id_or_alias_or_prefix
		best_match = None
		room = self.get_room(id_or_alias_or_prefix)
		if room is None: return id_or_alias_or_prefix
		for alias in itertools.chain((room.canonical_alias,), room.aliases):
			if alias is None: continue
			best_match = self._best_handle(best_match, alias)
			m = self.RE_PREFIX.search(alias)
			if not m: continue # This really should never happen, but.
			prefix = m.group(1)
			if self.get_room(prefix) is not None:
				# prefix _uniquely_ identifies the room
				best_match = self._best_handle(best_match, prefix)
		if best_match is None: return id_or_alias_or_prefix
		return best_match


class NoSyncMatrixClient(matrix_client.client.MatrixClient):
	# A subclass of MatrixClient that inhibits syncing until we allow it.

	# This class exists because we want to do some modifications to the
	# object (fixup) before _sync() is called for the first time.

	def __init__(self, *args, **kwargs):
		self.sync_attempted = False
		self.sync_enabled = False
		sync_filter = kwargs.pop('sync_filter', None)
		matrix_client.client.MatrixClient.__init__(self, *args, **kwargs)
		if sync_filter: self.sync_filter = sync_filter

	def _sync(self, *args, **kwargs):
		self.sync_attempted = True
		self.sync_args = args
		self.sync_kwargs = kwargs
		if self.sync_enabled:
			matrix_client.client.MatrixClient._sync(self, *args, **kwargs)

	def enable_sync(self):
		self.sync_enabled = True

	def finish_fixup(self):
		# This basically enables syncing, and calls the real _sync if
		# and only if it would have been called by the constructor.

		self.enable_sync()
		if self.sync_attempted:
			self._sync(*self.sync_args, **self.sync_kwargs)


class Message:
	def __init__(self, endpoint, *args, **kwargs):
		self.endpoint = endpoint
		self.args = args
		self.kwargs = kwargs

	def send(self):
		self.endpoint(*self.args, **self.kwargs)

	def __repr__(self):
		txtargs = [getattr(self.endpoint, '__qualname__', repr(self.endpoint))]
		txtargs.extend(map(repr, self.args))
		txtargs.extend("{}={!r}".format(x, y) for (x, y) in self.kwargs.items())
		return "Message({})".format(", ".join(txtargs))


class MessageSender:
	def __init__(self, sleep_time=5):
		self.sleeptime = sleep_time
		self.sendq = queue.Queue()

	def send(self, message):
		""" Send a message of type "Message" """

		notifier.notify(__name__, 'mcc.ms.send', message)
		return self.sendq.put(message)

	def sendthread(self):
		while True:
			try:
				message = self.sendq.get()
				notifier.notify(__name__, 'mcc.ms.sendthread.send', message)
				message.send()
			except queue.Empty:
				print("Queue was empty")
			time.sleep(self.sleeptime)

	def start_sendthread(self):
		t = threading.Thread(target=self.sendthread)
		t.daemon = True
		t.start()


class MXBaseClient:
	def __init__(self, accountfilename=None, account=None, sync_filter=None, sdkclient_factory=NoSyncMatrixClient):
		self.accountfilename = accountfilename
		self.account = account
		self.sdkclient = None
		self.sync_filter = sync_filter
		self.initial_sync_timeout_seconds = 600
		self.sync_timeout_seconds = 100
		self.sendcmd = None # deprecated
		self.messagesender = self.make_messagesender()
		self.sdkclient_factory = sdkclient_factory
		self.exceptionhandler = ExceptionHandler()

	def on_exception(self, *args, **kwargs):
		return self.exceptionhandler.on_exception(*args, **kwargs)

	def make_messagesender(self, *args, **kwargs):
		return MessageSender(*args, **kwargs)

	@wrap_exception
	def on_m_room_aliases(self, event):
		self.exceptionhandler.last_event = event
		if 'state_key' not in event: return # not a state event!
		self.rooms = RoomList(self.sdkclient.get_rooms())

	on_m_room_canonical_alias = on_m_room_aliases

	def send(self, message):
		""" Send a message of type "Message" """

		notifier.notify(__name__, 'mcc.mxc.send', message)
		return self.messagesender.send(message)

	def sendmsg(self, room_id, msg):
		""" Send message to room.

		Deprecated. Use self.send with the Message class instead. """

		notifier.notify(__name__, 'mcc.mxc.sendmsg', msg)
		return self.send(Message(self.sendcmd, room_id, msg))

	def start_send_thread(self, sendcmd, send_sleep_time=5):
		""" Deprecated. Use self.start_sendthread """

		self.sendcmd = sendcmd
		self.messagesender = self.make_messagesender(sleep_time=send_sleep_time)
		self.messagesender.start_sendthread()

	def start_sendthread(self):
		self.messagesender.start_sendthread()

	def hook(self):
		# Connect all the listeners, start threads etc.
		m = getattr(self, 'on_global_timeline_event', None)
		if callable(m): self.sdkclient.add_listener(m)
		m = getattr(self, 'on_m_room_canonical_alias', None)
		if callable(m): self.sdkclient.add_listener(m, 'm.room.canonical_alias')
		m = getattr(self, 'on_m_room_aliases', None)
		if callable(m): self.sdkclient.add_listener(m, 'm.room.aliases')
		m = getattr(self, 'on_exception', None)
		if callable(m): self.sdkclient.start_listener_thread(exception_handler=m)
		else: self.sdkclient.start_listener_thread()
		# Only supported by urllib-requests-adapter. NOOP otherwise.
		requests.GLOBAL_TIMEOUT_SECONDS = self.sync_timeout_seconds

	def login(self):
		# Only supported by urllib-requests-adapter. NOOP otherwise.
		requests.GLOBAL_TIMEOUT_SECONDS = self.initial_sync_timeout_seconds

		self._ensure_account()
		t = self.account.login_type()
		notifier.notify(__name__, 'mcc.mxc.login.connect', (self.account.hs_client_api_url, self.account.mxid))
		if t == self.account.T_PASSWORD:
			self.sdkclient = self.sdkclient_factory(self.account.hs_client_api_url, sync_filter=self.sync_filter)
			notifier.notify(__name__, 'mcc.mxc.login.login', (self.account.mxid))
			token = self.sdkclient.login_with_password(self.account.mxid, self.account.password)
			self.account.access_token = token
			self.account.mxid = self.sdkclient.user_id
			self.account.savetofile(self.accountfilename)
		elif t == self.account.T_TOKEN:
			self.sdkclient = self.sdkclient_factory(
				self.account.hs_client_api_url,
				token=self.account.access_token,
				user_id=self.account.mxid,
				sync_filter=self.sync_filter)
		else:
			raise CFException("MXClient.login(): Cannot login: 'account' is (partially) uninitialized")
		self.sdkclient.enable_sync()

	def first_sync(self):
		notifier.notify(__name__, 'mcc.mxc.first_sync.sync')
		self.sdkclient.finish_fixup()
		notifier.notify(__name__, 'mcc.mxc.first_sync.sync_done')
		self.rooms = RoomList(self.sdkclient.get_rooms())

	def _ensure_account(self):
		account = self.account
		if account is None:
			if self.accountfilename is None:
				raise CFException("MXClient.login(): neither 'accountfilename' nor 'account' given")
			account = AccountInfo()
			try:
				account.loadfromfile(self.accountfilename)
			except IOError as e:
				if e.errno != 2: # 2 = File Not Found
					raise
				while True:
					account.getfromkeyboard()
					if account: break
					print("You didn't enter all information correctly. Please try again.")

		self.account = account


class MXClient(MXBaseClient, ConsoleClientMixin):
	def __init__(self, *args, **kwargs):
		MXBaseClient.__init__(self, *args, **kwargs)
		ConsoleClientMixin.__init__(self)
